"use strict" ;

// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

//Для створення вузлів DOM існують два методи: 
// document.createElement(tag) - Створює новий вузол елемента з
// вказаним тегом:
// let div = document.createElement('div') ;

// document.createTextNode(text) - Створює новий вузол тексту із заданим текстом
// let TextNode = document.createTextNode ('Some text')

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

// const navigationElement = document.querySelector('.navigation'); //Спочатку потрібно визначити елемент, який треба видалити
// navigationElement.remove(); // Після того як визначили елемент який треба видалити, треба використовувати метод remove() для його видалення
// const headerElement = document.querySelector('header');
// console.log(headerElement.innerHTML);
// navigationElement.remove();
// console.log(headerElement.innerHTML);  // Це виводить вміст елемента header на консоль до і після видалення елемента navigation, що дає змогу переконатися що елемент був видалений.
  
// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

// prepend() - використовується для вставки одного чи більше вузлів перед дочірніми елементами вказаного батьківського елемента.
// append() - використовується для вставки одного чи більше вузлів після дочірніх елементів вказаного батьківського елемента. 
// before() - використовується для вставки одного чи більше вузлів перед вказаним елементом, який є дочірнім для спільного батька. 
// after() - використовується для вставки одного чи більше вузлів після вказаного елемента, який є дочірнім для спільного батька.

// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const footer = document.querySelector('footer');
const paragraph = footer.querySelector('p');

const newElement = document.createElement('a');
newElement.textContent = 'Learn More';
newElement.href = '#';

footer.insertBefore(newElement, paragraph.nextElementSibling);

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const main = document.querySelector('main');
const featuresSection = document.querySelector('section:nth-child(2)');
const ratingSelect = document.createElement('select');
ratingSelect.id = 'rating';

main.insertBefore(ratingSelect, featuresSection);

const ratingOptions = ['4 Stars', '3 Stars', '2 Stars', '1 Star'];

for (const [index, value] of ratingOptions.entries()) {
  const optionElement = document.createElement('option');
  optionElement.value = index + 1;
  optionElement.textContent = value;
  ratingSelect.appendChild(optionElement);
}